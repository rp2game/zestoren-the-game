import json
from Zestoren.CenasJson.acoes import Acoes
from Zestoren.CenasJson.locomocao import Locomocao
from Zestoren.CenasJson.consequencia import Consequencia
from Zestoren.CenasJson.prox_acao import ProximaAcao
from Zestoren.CenasJson.cenas import Cenas
from Zestoren.CenasJson.capitulos import Capitulos

# json_data = open('prologue.json')
# data = json.load(json_data)
# prologue = data["vetor"]
# print(prologue[0]["prologue"])
json_data = open("capitulo1.json")
data = json.load(json_data)


def carregarJason():
    my_dict = {}
    for cenas in data["cenas"]:
        cenasN = cenas["cenaN"]
        descricao = cenas["descricao"]
        skip = cenas["skip"]
        acoes = {}
        prox_acoes = {}
        consequencias = {}
        locomocoes = {}

        for acao in cenas["acoes"]:
            nome_acao = acao["acaoN"]
            descricao = acao["descricao"]
            prox_cena = acao["prox_cena"]
            acoes[nome_acao] = Acoes(nome_acao, descricao, prox_cena)

        for prox_acao in cenas["prox_acao"]:
            prox_acaoN = prox_acao["prox_acaoN"]
            descricaoP = prox_acao["descricao"]
            consequenciaL = prox_acao["consequencia"]
            prox_acoes[prox_acaoN] = ProximaAcao(prox_acaoN, descricaoP, consequenciaL)

        for consequencia in cenas["consequencia"]:
            consequenciaN = consequencia["consequenciaN"]
            skip = consequencia["skip"]
            cena = consequencia["cena"]
            consequencias[consequenciaN] = Consequencia(consequenciaN, skip, cena)

        for locomocao in cenas["locomocao"]:
            locomocaoN = locomocao["locomocaoN"]
            descricaoL = locomocao["descricao"]
            proxima_cena = locomocao["proxima_cena"]
            locomocoes[locomocaoN] = Locomocao(locomocaoN, descricaoL, proxima_cena)
        my_dict[cenasN] = Cenas(cenasN, descricao, skip, acoes, prox_acoes, consequencias, locomocoes)
        return my_dict
    # while (!fimjogo):
    #   blocoatual=blocos[initial]
