# encoding: utf-8
# encoding: iso-8859-1
# encoding: win-1252
from Zestoren.Personagens.cientista import Cientista, Engenheiro, Bioquimico, Medico
from Zestoren.Personagens.militar import Militar, Comando, Sniper, Especialista
from Zestoren.Personagens.religioso import Religioso, Xama, Monge, Padre
from Zestoren.Principal.validacao import validacaoIntString

dir()


def main():
    centralizando1 = '*******************************************************'
    centralizando2 = 'Zerstören: The Game'
    centralizando3 = '*******************************************************'
    print(centralizando1.center(50))
    print(centralizando2.center(53))
    print(centralizando3.center(50))
    # mixer.music.play()
    iniciar_jogo = input(
        '\nDigite "Iniciar" para começar a sua jornada ou "Sair".' + '\n')
    while iniciar_jogo.lower() != 'iniciar' and iniciar_jogo.lower() != 'sair':
        print("Escolha inválida")
        iniciar_jogo = input(
        'Digite "Iniciar" para começar a sua jornada ou "Sair".' + '\n')
    # if "Iniciar" in iniciar_jogo: - USAR ISSO NO FIM DA INTRO -
    #  mixer.music.stop()
    if iniciar_jogo.lower() == 'iniciar':
        print("Qual o seu nome?")
        nome_do_personagem = input()
        print('O que você é?')
        possible_classes = [Cientista, Militar, Religioso]
        possible_subclasses = [[Medico, Engenheiro, Bioquimico],
                               [Comando, Sniper, Especialista],
                               [Xama, Padre, Monge]]
        for i, cls in enumerate(possible_classes):
            print("{} - {}".format(i + 1, cls.descricao))
        escolhas_de_classes = validacaoIntString()
        while 1 < escolhas_de_classes > len(possible_classes):
            print("Pressione uma tecla válida!")
            escolhas_de_classes = validacaoIntString()


        subclasses = possible_subclasses[escolhas_de_classes - 1]

        print("Selecione uma especialidade:")
        for i, subclass in enumerate(subclasses):
            print("{} - {}".format(i + 1, subclass.__name__))
        escolhas_de_especialidades = int(input())
        while 1 < escolhas_de_especialidades > len(subclasses) and escolhas_de_especialidades == 0:
            print("Pressione uma tecla válida!")
            escolhas_de_especialidades = int(input())


        subclasse_escolhida = subclasses[escolhas_de_especialidades - 1]
        jogador = subclasse_escolhida()
        print(jogador)



        
      #while(1<2):
      #cena_atual.descricao

    elif iniciar_jogo.lower() == 'sair':
        print('Até mais!')


if __name__ == '__main__':
    main()

