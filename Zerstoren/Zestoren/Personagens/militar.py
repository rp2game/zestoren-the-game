from Zestoren.Personagens.personagens import *
from random import *



class Militar(Personagens):
    descricao = "Militar (O furor de um guerreiro brinda coragem, e mais nada.)"

    def __init__(self, mobilidade, hp):
        super(Militar, self).__init__(100, 50)
        self.hp = hp
        self.mobilidade = mobilidade


class Comando(Militar):
    def __init__(self):
        super(Comando, self).__init__(50, 100)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)


    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida do comando:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do comando:" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Pode carregar rifles e submetralhadoras."


class Especialista(Militar):
    def __init__(self):
        super(Especialista, self).__init__(65, 70)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)




    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida do especialista:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do especialista:" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Pode carregar armamentos pesados."


class Sniper(Militar):
    def __init__(self):
        super(Sniper, self).__init__(80, 50)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)

    def probabilidade_de_acerto(self, dano):
        acerto = 100 * uniform(0.1, 0.6)
        if acerto >= 60:
            return self.ataque_bom(dano)
        elif acerto < 60:
            return self.ataque_ruim(dano)

    def ataque_ruim(self, ataque_ruim):
        self.hp = self.hp - (ataque_ruim - randint(0, 4))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida do sniper:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do sniper:" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Pode carregar pistolas e rifles de precisão."