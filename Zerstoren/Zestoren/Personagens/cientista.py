from Zestoren.Personagens.personagens import *
from random import *


class Cientista(Personagens):
    descricao = "Cientista (Ter medo do que não se entende é perfeitamente natural quando seus olhos estão apenas naquilo que pode ser compreendido.)"

    def __init__(self, mobilidade, hp):
        super(Cientista, self).__init__(50, 75)
        self.hp = hp
        self.mobilidade = mobilidade


class Medico(Cientista):
    def __init__(self):
        super(Medico, self).__init__(50, 50)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)

    def probabilidade_de_acerto(self, dano):
        acerto = 100 * uniform(0.1, 0.6)
        if acerto >= 60:
            return self.ataque_bom(dano)
        elif acerto < 60:
            return self.ataque_ruim(dano)

    def ataque_ruim(self, ataque_ruim):
        self.hp = self.hp - (ataque_ruim - randint(0, 4))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida do médico:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do médico:" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Capaz de manipular medicamentos e curar ferimentos temporariamente." + '\n'


class Engenheiro(Cientista):
    def __init__(self):
        super(Engenheiro, self).__init__(65, 50)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)

    def probabilidade_de_acerto(self, dano):
        acerto = 100 * uniform(0.1, 0.6)
        if acerto >= 60:
            return self.ataque_bom(dano)
        elif acerto < 60:
            return self.ataque_ruim(dano)

    def ataque_ruim(self, ataque_ruim):
        self.hp = self.hp - (ataque_ruim - randint(0, 4))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida do engenheiro:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do engenheiro:" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Capaz de construir armas."


class Bioquimico(Cientista):
    def __init__(self):
        super(Bioquimico, self).__init__(50, 70)

    def aumento_m(self, multiplicador_m):
        self.medo = self.medo + self.resistenciaM * multiplicador_m
        if self.medo <= 30:
            return "Seu nível de medo está baixo! \nSeu nível de medo é: " + str(self.medo)
        elif 30 < self.medo <= 60:
            return "Seu nível de medo está alto! \nSeu nível de medo é: " + str(self.medo)
        elif 60 < self.medo < 100:
            return "Seu nível de medo está perigosamente alto! \nSeu nível de medo é: " + str(self.medo)
        elif self.medo == 100:
            self.hp = self.hp - (self.hp * multiplicador_m)
            return "Seu nível de medo chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \n" \
                   "Sua vida atual é de: " + str(self.hp)

    def aumento_ins(self, multiplicador_ins):
        self.insanidade = self.insanidade + self.resistenciaIns * multiplicador_ins
        if self.insanidade <= 30:
            return "Seu nível de insanidade está baixo! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 30 < self.insanidade <= 60:
            return "Seu nível de insanidade está alto! \nSeu nível de insanidade é: " + str(self.insanidade)
        elif 60 < self.insanidade < 100:
            return "Seu nível de insanidade está perigosamente alto! \nSeu nível de insanidade é: " + str(
                self.insanidade)
        elif self.insanidade == 100:
            self.hp = self.hp - (self.hp * multiplicador_ins)
            return "Seu nível de insanidade chegou ao máximo! \nTodo seu dano tomado será redirecionado ao seu HP! \nSua vida atual é de: " + str(
                self.hp)

    def probabilidade_de_acerto(self, dano):
        acerto = 100 * uniform(0.1, 0.6)
        if acerto >= 60:
            return self.ataque_bom(dano)
        elif acerto < 60:
            return self.ataque_ruim(dano)

    def ataque_ruim(self, ataque_ruim):
        self.hp = self.hp - (ataque_ruim - randint(0, 4))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def ataque_bom(self, ataque_bom):
        self.hp = self.hp - (ataque_bom - randint(5, 9))
        return "Voce tomou dano! Sua vida atual é de: " + str(self.hp)

    def __str__(self):
        return "Pontos de vida da classe bioquimico:" + '\n' + str(
            self.hp) + '\n' + "Mobilidade do bioquimico" + '\n' + str(
            self.mobilidade) + '\n' + "Resistencia ao medo:" + '\n' + str(
            self.resistenciaM) + '\n' + "Resistencia a insanidade:" + '\n' + str(
            self.resistenciaIns) + '\n' + "Atributos únicos da classe:" + '\n' + "Capaz de criar bombas e medicamentos."